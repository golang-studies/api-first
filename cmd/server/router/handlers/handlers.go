package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/golang-studies/api-first/internal/pkg/people"
)

type Handlers struct {
	people *people.People
}

func NewHandlers() *Handlers {
	return &Handlers{
		people: people.NewPeople(),
	}
}

func (h *Handlers) CreatePerson(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method invalid. required method POST", http.StatusMethodNotAllowed)
		return
	}

	rsp, err := h.people.CreatePerson(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	rspJson, err1 := json.Marshal(rsp)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write(rspJson)
	// json.NewEncoder(w).Encode(rsp)
}

func (h *Handlers) GetPersonId(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method invalid. required method Get", http.StatusMethodNotAllowed)
		return
	}

	id, err := strconv.Atoi(r.Header.Get("id"))
	if err != nil {
		http.Error(w, "incorrect id param", http.StatusBadRequest)
		return
	}

	rsp, err := h.people.GetPersonId(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	rspJson, err1 := json.Marshal(rsp)
	if err1 != nil {
		http.Error(w, err1.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(rspJson)
}
